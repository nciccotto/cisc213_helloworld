/* CISC213 Hello World Project
Nicholas Ciccotto 01/19/2023 */


public class HelloWorld {
	public static void main(String[]args) {
		Person myperson = new Person();
		System.out.println("Hello World");
		myperson.setFirstName("Nick");
		myperson.setLastName("Ciccotto");
		System.out.println(myperson.toString());
	}//end main
}//end class
